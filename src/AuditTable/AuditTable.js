import Checkbox from "@material-ui/core/Checkbox";
import Paper from "@material-ui/core/Paper";
import { lighten, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import PropTypes from "prop-types";
import React from "react";
import "./AuditTable.scss";

//images
import iconXls from "./../public/images/Content/ic_xls.svg";

// import DeleteIcon from '@material-ui/icons/Delete';
// import FilterListIcon from '@material-ui/icons/FilterList';

// class AuditTable extends React.Component {
//   constructor(props) {
//     super(props);
//     console.log(props);
//     this.state = {};
//   }
// }
// export default AuditTable;
// console.log(localStorage.getItem("Audit_Trail"));
const rows = [
  {
    description: "Product_Name",
    jumbosku: "AHJYY89",
    jumboprice: "Jumbo_Price",
    oldprice: "Old_Price",
    newprice: "New_Price",
    per_modified: 5,
    floorlimit: 3100.25,
    datemodified: "Date_Modified",
    timemodified: "Time_Modified",
    modifiedb: "Username"
  }
  // Product_Name
  // Time_Modified
  // Old_Price number
  // New_Price number
  // Jumbo_Price number
  // Date_Modified
];

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === "desc"
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const JSONToCSVConvertor = (JSONData, fileName, ShowLabel, selected) => {
  let JSONDatas = [];
  let description = "PRODUCT DESCRIPTION".bold();

  // let bold = description.;

  console.log(description);

  JSONData.map(data => {
    if (selected.indexOf(data.index) !== -1) {
      console.log(data.description);
      // console.log(typeof data.description);

      JSONDatas.push({
        "Product Description": data.description,
        "Jumbo SKU": data.jumbosku,
        "Cost Price": data.jumboprice,
        "Current Price": data.oldprice,
        "New Price": data.newprice,
        "Per Modified": data.per_modified,
        "Floor Limit": data.floorlimit,
        "Date Modified": data.datemodified,
        "Time Modified": data.timemodified

        // modifiedb: "Username"
      });
    }
  });
  if (JSONDatas.length == 0) {
    alert("Please select checkbox");
    return false;
  }
  //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
  var arrData =
    typeof JSONDatas != "object" ? JSON.parse(JSONDatas) : JSONDatas;
  var CSV = "";
  //Set Report title in first row or line
  // CSV += ReportTitle + "\r\n\n";
  //This condition will generate the Label/Header
  if (ShowLabel) {
    var row = "";
    //This loop will extract the label from 1st index of on array
    for (var index in arrData[0]) {
      //Now convert each value to string and comma-seprated
      row += index + ",";
    }
    row = row.slice(0, -1);
    //append Label row with line break
    CSV += row + "\r\n";
  }
  //1st loop is to extract each row
  for (var i = 0; i < arrData.length; i++) {
    var row = "";
    //2nd loop will extract each column and convert it in string comma-seprated
    for (var index in arrData[i]) {
      row += '"' + arrData[i][index] + '",';
    }
    row.slice(0, row.length - 1);
    //add a line break after each row
    CSV += row + "\r\n";
  }
  if (CSV == "") {
    alert("Invalid data");
    return;
  }
  //Generate a file name
  // var fileName = "MyReport_";
  //this will remove the blank-spaces from the title and replace it with an underscore
  // fileName += ReportTitle.replace(/ /g, "_");
  //Initialize file format you want csv or xls
  var uri = "data:text/csv;charset=utf-8," + escape(CSV);
  // Now the little tricky part.
  // you can use either>> window.open(uri);
  // but this will not work in some browsers
  // or you will not get the correct file extension
  //this trick will generate a temp <a /> tag
  var link = document.createElement("a");
  link.href = uri;
  //set the visibility hidden so it will not effect on your web-layout
  link.style = "visibility:hidden";
  link.download = fileName + ".csv";
  //this part will append the anchor tag and remove it after automatic click
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};

const headCells = [
  {
    id: "description",
    numeric: false,
    disablePadding: true,
    label: "PRODUCT DESCRIPTION",
    textalignleft: true
  },
  {
    id: "jumbosku",
    numeric: false,
    disablePadding: false,
    label: "Jumbo\r\nSKU",
    textalignleft: false
  },
  {
    id: "jumboprice",
    numeric: true,
    disablePadding: false,
    label: "Cost\r\nPrice",
    textalignleft: false
  },
  {
    id: "oldprice",
    numeric: true,
    disablePadding: false,
    label: "Current\r\nPrice",
    textalignleft: false
  },
  {
    id: "newprice",
    numeric: true,
    disablePadding: false,
    label: "New\r\nPrice",
    textalignleft: false
  },
  {
    id: "per_modified",
    numeric: true,
    disablePadding: false,
    label: "%\r\nModified",
    textalignleft: false
  },
  {
    id: "floorlimit",
    numeric: true,
    disablePadding: false,
    label: "Floor\r\nLimit",
    textalignleft: false
  },

  {
    id: "datemodified",
    numeric: false,
    disablePadding: false,
    label: "Date\r\nModified",
    textalignleft: false
  },
  {
    id: "timemodified",
    numeric: false,
    disablePadding: false,
    label: "Time\r\nModified",
    textalignleft: false
    // },
    // {
    //   id: "modifiedby",
    //   numeric: false,
    //   disablePadding: false,
    //   label: "Modified by",
    //   textalignleft: false
  }
];

function EnhancedTableHead(props) {
  const {
    classes,
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort
  } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };
  // console.log(classes);
  //   const createSortHandler = property => event => {
  //     onRequestSort(event, property);
  //   };

  return (
    <TableHead className={classes.tablehead}>
      <TableRow>
        <TableCell padding="checkbox" className={classes.tableheadcheckbox}>
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={numSelected === rowCount}
            className={classes.checkbox}
            inputProps={{ "aria-label": "select all desserts" }}
            onChange={onSelectAllClick}
          />
        </TableCell>
        {headCells.map(headCell => (
          <TableCell
            className={classes.tabletitle}
            key={headCell.id}
            align={headCell.textalignleft ? "left" : "center"}
            padding={headCell.disablePadding ? "none" : "default"}
          >
            {headCell.label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  rowCount: PropTypes.number.isRequired
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1)
  },
  highlight:
    theme.palette.type === "light"
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85)
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark
      },
  spacer: {
    flex: "1 1 100%"
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: "0 0 auto"
  }
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return "";
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired
};

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3)
  },
  toolbar: {
    boxShadow: "none"
  },
  tablehead: {
    background: "#e2e9f05c",
    "& tr th:nth-child(2)": {
      borderLeft: "none"
    }
  },
  tableheadcheckbox: {
    borderBottom: "none",
    borderLeft: "none",
    padding: "10px"
  },
  tabletitle: {
    fontWeight: "bold",
    color: "#174a84",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px",
    whiteSpace: "pre-line",
    textAlign: "center"
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
    boxShadow: "none"
  },
  table: {
    minWidth: 750
  },
  row: {
    borderBottom: "none"
  },
  checkboxcell: {
    borderBottom: "none",
    minWidth: "40px",
    padding: "10px"
  },
  checkbox: {
    width: "13px",
    height: "13px",
    borderRadius: "3px",
    background: "#fff",
    color: "#0086f8 !important",
    borderBottom: "none"
  },
  description: {
    fontWeight: "500",
    color: "#164883",
    borderBottom: "none",
    padding: "10px"
  },
  oldprice: {
    fontWeight: "500",
    textAlign: "center",
    color: "#164883",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  jumbosku: {
    fontWeight: "500",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  jumboprice: {
    fontWeight: "500",
    textAlign: "center",
    color: "#00c853",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  newprice: {
    fontWeight: "500",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  per_modified: {
    fontWeight: "500",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  floorlimit: {
    fontWeight: "500",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  datemodified: {
    fontWeight: "500",
    textAlign: "center",
    color: "#164883",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  timemodified: {
    fontWeight: "500",
    textAlign: "center",
    color: "#164883",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  modifiedby: {
    fontWeight: "500",
    textAlign: "center",
    color: "#164883",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  tableWrapper: {
    overflowX: "auto"
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1
  }
}));
function deepClone(obj, hash = new WeakMap()) {
  if (Object(obj) !== obj) return obj; // primitives
  if (obj instanceof Set) return new Set(obj); // See note about this!
  if (hash.has(obj)) return hash.get(obj); // cyclic reference
  const result =
    obj instanceof Date
      ? new Date(obj)
      : obj instanceof RegExp
        ? new RegExp(obj.source, obj.flags)
        : obj.constructor
          ? new obj.constructor()
          : Object.create(null);
  hash.set(obj, result);
  if (obj instanceof Map)
    Array.from(obj, ([key, val]) => result.set(key, deepClone(val, hash)));
  return Object.assign(
    result,
    ...Object.keys(obj).map(key => ({ [key]: deepClone(obj[key], hash) }))
  );
}
export default function EnhancedTable(props) {
  // const a = props.;
  let rows = [];
  console.log();
  // console.log(props.data);
  let ri = 0;
  let className = "export_btn",
    filename = "Audit Trail - " + props.data.selectedCategory;
  // console.log(props.data.products);
  props.data.products.map(pro => {
    pro.Audit_Trail.map(x => {
      rows.push({
        index: ri++,
        description: pro.Product_Name,
        jumbosku: pro.SKU,
        jumboprice: x.Jumbo_Price,
        oldprice: x.Old_Price,
        newprice: x.New_Price,
        per_modified: x.Modified_percentage,
        floorlimit: x.Floor_Limit,
        datemodified: x.Date_Modified,
        timemodified: x.Time_Modified
        // modifiedb: "Username"
      });
    });
  });

  // console.log(deepClone(rows));
  // const rows = [
  //   {
  //     description: "Product_Name",
  //     jumbosku: "AHJYY89",
  //     jumboprice: "Jumbo_Price",
  //     oldprice: "Old_Price",
  //     newprice: "New_Price",
  //     per_modified: 5,
  //     floorlimit: 3100.25,
  //     datemodified: "Date_Modified",
  //     timemodified: "Time_Modified",
  //     modifiedb: "Username"
  //   }
  //   // Product_Name
  //   // Time_Modified
  //   // Old_Price number
  //   // New_Price number
  //   // Jumbo_Price number
  //   // Date_Modified
  // ];
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("description");
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  //   const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleRequestSort = (event, property) => {
    const isDesc = orderBy === property && order === "desc";
    setOrder(isDesc ? "asc" : "desc");
    setOrderBy(property);
  };

  const handleSelectAllClick = event => {
    if (event.target.checked) {
      const newSelecteds = rows.map(n => n.index);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, description) => {
    const selectedIndex = selected.indexOf(description);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, description);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    console.log(event);
    setPage(newPage);
    props.onpageSetFalse(newPage);
  };

  const handleChangeRowsPerPage = event => {
    console.log(event);
    setRowsPerPage(+event.target.value);
    setPage(0);
    props.onpageSetFalse(0);
  };

  //   const handleChangeDense = event => {
  //     setDense(event.target.checked);
  //   };

  const isSelected = description => selected.indexOf(description) !== -1;

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  const [pageSizes] = React.useState([5, 10, 15, 0]);
  if (props.data.page != page) {
    handleChangePage(null, 0);
  }
  return (
    <div className="row">
      <div
        className="col text-center"
        style={{ display: rows.length > 0 ? "none" : "block" }}
      >
        No Data Found
      </div>
      <div
        className="table-main col"
        style={{ display: rows.length == 0 ? "none" : "flex" }}
      >
        <Paper className={classes.paper}>
          <EnhancedTableToolbar
            numSelected={selected.length}
            className={classes.toolbar}
          />
          <div className={classes.tableWrapper}>
            {/* datemodified */}
            <Table className={classes.table} aria-labelledby="tableTitle">
              <EnhancedTableHead
                classes={classes}
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}
              />
              <TableBody>
                {stableSort(rows, getSorting(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    const isItemSelected = isSelected(row.index);
                    const labelId = `enhanced-table-checkbox-${row.index}`;

                    return (
                      <TableRow
                        hover
                        onClick={event => handleClick(event, row.index)}
                        role="checkbox"
                        datemodified
                        aria-checked={isItemSelected}
                        tabIndex={-1}
                        key={row.index}
                        selected={isItemSelected}
                        className={classes.row}
                        style={
                          index % 2
                            ? { background: "#e2e9f05c" }
                            : { background: "#fff" }
                        }
                      >
                        <TableCell
                          padding="checkbox"
                          className={classes.checkboxcell}
                        >
                          <Checkbox
                            className={classes.checkbox}
                            checked={isItemSelected}
                            inputProps={{ "aria-labelledby": labelId }}
                          />
                        </TableCell>
                        <TableCell
                          className={classes.description}
                          component="th"
                          id={labelId}
                          scope="row"
                          padding="none"
                        >
                          {row.description}
                        </TableCell>

                        <TableCell align="right" className={classes.jumbosku}>
                          {row.jumbosku}
                        </TableCell>
                        <TableCell align="right" className={classes.jumboprice}>
                          {row.jumboprice}
                        </TableCell>
                        <TableCell align="right" className={classes.oldprice}>
                          {row.oldprice}
                        </TableCell>
                        <TableCell align="right" className={classes.newprice}>
                          {row.newprice}
                        </TableCell>
                        <TableCell
                          align="right"
                          className={classes.per_modified}
                        >
                          {row.per_modified}
                        </TableCell>
                        <TableCell align="right" className={classes.floorlimit}>
                          {row.floorlimit}
                        </TableCell>
                        <TableCell
                          align="right"
                          className={classes.datemodified}
                        >
                          {row.datemodified}
                        </TableCell>
                        <TableCell
                          align="right"
                          className={classes.timemodified}
                        >
                          {row.timemodified}
                        </TableCell>
                        {/* <TableCell align="right" className={classes.modifiedby}>
                          {row.modifiedby}
                        </TableCell> */}
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </div>

          <div className="row align-items-center text-right">
            <div className="col">
              {/* <JsonToExcel
                data={dataE}
                className={className}
                filename={filename}
                fields={fields}
                style={style}
              /> */}
              <span
                className="export_btn"
                onClick={e =>
                  JSONToCSVConvertor(rows, filename, true, selected)
                }
              >
                <img src={iconXls} className="align-text-top pr-2" />
                EXPORT REPORT
              </span>
            </div>

            <TablePagination
              rowsPerPageOptions={[10, 20, 30]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                "aria-label": "previous page"
              }}
              nextIconButtonProps={{
                "aria-label": "next page"
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </div>
        </Paper>
      </div>
    </div>
  );
}
