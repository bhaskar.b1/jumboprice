import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
// import { makeStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
// you will also need the css that comes with bootstrap-daterangepicker
import "bootstrap-daterangepicker/daterangepicker.css";
// you will need the css that comes with bootstrap@3. if you are using
// a tool like webpack, you can do the following:
import "bootstrap/dist/css/bootstrap.css";
// chart
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import Exporting from "highcharts/modules/exporting";
import moment from "moment";
import React, { Component } from "react";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { Tab, TabList, TabPanel, Tabs } from "react-web-tabs";
import "react-web-tabs/dist/react-web-tabs.css";
import AuditTable from "../AuditTable/AuditTable";
import SidebarMenu from "../SidebarMenu/SidebarMenu";
import Table from "../Table/Table";
import icClose from "./../public/images/Content/error.svg";
import icSearch from "./../public/images/Content/icSearch.svg";
// import pricing_active from './../public/images/Content/pricing_active.svg';
// images
import icCal from "./../public/images/Content/ic_cal.svg";
import "./Content.scss";
Exporting(Highcharts);
// let minRate = 120;
// milliseconds

//INTERVALS TO
let Ginterval = 86400000;
let Gday = 1;
let Gmonth = 0;
let Gyear = 2019;


const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  MenuItem: {
    background: "transparent !important",
    fontWeight: "normal",
    color: "#164883",
    borderBottom: "0.5px solid #00accd",
    paddingLeft: 0,
    paddingRight: 0,
    margin: "0px 15px",
    "&:hover": {
      background: "transparent"
    },
    "&:last-child": {
      borderBottom: "none"
    }
  },
  MenuItemSelected: {
    background: "transparent",
    fontWeight: 600,
    color: "#164883",
    borderBottom: "0.5px solid #00accd"
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  }
});

class Content extends Component {
  // componentDidUpdate() {
  //   let x = 0;
  //   if (x === 0) {
  //     console.log("change");
  //     this.handleChange(this.state.selected);
  //     x++;
  //   }
  // }
  constructor(props) {
    super(props);
    this.state = {
      selected: 0,
      CostPrice: 0,
      page: 0,
      apage: 0,
      pricingselected: "",
      hasError: false,
      productName: "",
      productSku: "",
      options: {
        title: {
          text: ""
        },
        xAxis: {
          type: "datetime"
        },
        yAxis: {
          title: {
            text: "Price (AED)"
          },
          plotLines: [
            {
              value: 0,
              color: "gray",
              dashStyle: "longdash",
              width: 1,
              label: {
                text: ""
              }
            }
          ]
        },

        // 24 * 3600000  1 DAY
        plotOptions: {
          series: {
            pointStart: Date.UTC(Gyear, Gmonth, Gday),
            pointInterval: Ginterval
          }
        },

        legend: {
          align: "right",
          verticalAlign: "top",
          layout: "vertical",
          // floating: true,
          position: "relative",
          useHTML: true,
          width: 100, // this width for the Jumbo indication of graph
          labelFormatter: function () {
            return (
              "<div style='background: " +
              this.color +
              ";float: left;width: 14px; height: 14px; border-radius: 2px;margin: 0px 5px 5px 5px;'> </div>" +
              this.name
            );
          },
          itemStyle: {
            color: "#164883",
            fontWeight: "normal",
            fontSize: "9px",
            textOverflow: ""
          },
          x: 0,
          y: 20
        },
        series: [
          {
            name: "",
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            type: "spline",
            color: "#315496",
            marker: {
              fillColor: "#55D8FE",
              symbol: "■"
            }
          },
          {
            name: "",
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            type: "spline",
            color: "#000000",
            marker: {
              fillColor: "#55D8FE",
              symbol: "■"
            }
          },
          {
            name: "",
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            type: "spline",
            color: "#fe0000",
            marker: {
              fillColor: "#55D8FE",
              symbol: "■"
            }
          },
          {
            name: "",
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            type: "spline",
            color: "#ffc000",
            marker: {
              fillColor: "#55D8FE",
              symbol: "■"
            }
          },
          {
            name: "",
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            type: "spline",
            color: "#ffff00",
            marker: {
              fillColor: "#55D8FE",
              symbol: "■"
            }
          }
        ],
        credits: {
          enabled: false
        },

        responsive: {
          rules: [
            {
              condition: {
                maxWidth: 1024
              },
              chartOptions: {
                legend: {
                  layout: "horizontal",
                  align: "center",
                  width: 400,
                  verticalAlign: "bottom"
                }
              }
            }
          ]
        },
        exporting: {
          filename: "",
          allowHTML: true /* true for the indication */,
          sourceWidth: 1500,
          sourceHeight: 450,
          printMaxWidth: 1500,
          buttons: {
            contextButton: {
              enabled: false,
              verticalAlign: "bottom",
              x: -10,
              y: -10
            },
            exportButton: {
              text: "Export Graph",
              theme: {
                "stroke-width": 1,
                stroke: "#3caccc",
                r: 0,
                states: {
                  hover: {
                    fill: "#3caccc"
                  },
                  select: {
                    stroke: "#039",
                    fill: "#3caccc"
                  }
                }
              },
              verticalAlign: "bottom",
              x: -10,
              y: -10,
              menuItems: [
                "downloadPNG",
                "downloadJPEG",
                "downloadPDF",
                "downloadSVG"
              ]
            }
          }
          // scale: 1
        }
      },
      productId: 0,
      products: [],
      auditData: [],
      compareData: [],
      pricingT: [],
      data: [],
      startDt: "MM/DD/YYYY",
      endDt: "MM/DD/YYYY",
      startDate: moment().subtract(1, "month"),
      endDate: moment(),
      selectedCategory: "",
      values: "",
      selectedBrand: ""
    };
  }

  componentDidMount() {
    this.loadProducts();
  }

  handleChange(value, CostPrice) {
    //console.log(this.state.selectedCategory);
    this.setState({ selected: value }, () => {
      if (value) {
        // console.log("CostPrice", CostPrice);
        if (this.state.products.some(i => i.Item_Number == value)) {
          let series = this.state.options.series;
          if (series != undefined) {

            for (let i = 0; i < series.length; i++) {
              this.state.products.map(val => {
                if (val.Item_Number) {
                  if (val.Item_Number == value) {
                    CostPrice = val.CostPrice;
                    // console.log("CostPrice", CostPrice, val);
                    if (val.Pricing_Trend[i].Competitor_Name !== undefined) {
                      series[i].name = val.Pricing_Trend[i].Competitor_Name;
                      // series[i].data = val.Pricing_Trend[i].Y_coordinates;
                      let Y_coordinates = [];
                      let xStart = Date.UTC(
                        +this.state.products.filter(
                          i => i.Item_Number == value
                        )[0].Pricing_Trend[0].X_coordinate.Year,
                        this.state.products.filter(
                          i => i.Item_Number == value
                        )[0].Pricing_Trend[0].X_coordinate.Month - 1,
                        this.state.products.filter(
                          i => i.Item_Number == value
                        )[0].Pricing_Trend[0].X_coordinate.Day
                      );
                      let xInterval = this.state.products.filter(
                        i => i.Item_Number == value
                      )[0].Pricing_Trend[0].X_coordinate.Interval;
                      val.Pricing_Trend[i].Y_coordinates.map(y => {
                        Y_coordinates.push([xStart, y]);
                        xStart += xInterval;
                      });
                      series[i].data = Y_coordinates;
                    }
                  }
                }
              });
            }

            this.setState({
              options: {
                series: series,
                exporting: {
                  filename: "Pricing Trends - " + this.state.selectedCategory
                },
                yAxis: {
                  plotLines: [
                    {
                      value: CostPrice,
                      dashStyle: "longdash",
                      color: "gray",
                      width: 1.5,
                      events: {
                        mouseover: function (e) {
                          var series = this.axis.series[0],
                            chart = series.chart,
                            PointClass = series.pointClass,
                            tooltip = chart.tooltip,
                            point = (new PointClass()).init(
                              series, ['Cost Price', this.options.value]
                            ),
                            normalizedEvent = chart.pointer.normalize(e);

                          point.tooltipPos = [
                            normalizedEvent.chartX - chart.plotLeft,
                            normalizedEvent.chartY - chart.plotTop
                          ];

                          tooltip.refresh(point);
                        },
                        mouseout: function (e) {
                          this.axis.chart.tooltip.hide();
                        }
                      },
                      label: {
                        text: ""
                      }
                    }
                  ]
                }
              }
            });
          }
        }
      }
    });
  }
  priceChange(value) {
    this.setState({ pricingselected: value });
  }

  handleClick() {
    this.setState({ hasError: false });
    if (!this.state.selected) {
      this.setState({ hasError: true });
    }
  }
  // handleEvent(event, picker) {
  //   console.log(picker.startDate);
  // }
  handleEvent(event, picker) {
    // console.log(picker.startDate._d);
    let s = new Date(picker.startDate._d).toLocaleDateString("en-US");
    let e = new Date(picker.endDate._d).toLocaleDateString("en-US");
    // console.log(s);
    // let startdate = s.split('/', 1);
    // let startmonth = s.split('/', 2);
    // let startyear = s.split('/', 3);
    // var startdate1 = startyear[0];
    // var startmonth1 = startyear[1];
    // var startyear1 = startyear[2];
    // let startfinal = startmonth1 + "/" + startdate1 + "/" + startyear1;

    // let enddate = e.split('/', 1);
    // let endmonth = e.split('/', 2);
    // let endyear = e.split('/', 3);
    // var enddate1 = endyear[0];
    // var endmonth1 = endyear[1];
    // var endyear1 = endyear[2];
    // let endfinal = endmonth1 + "/" + enddate1 + "/" + endyear1;
    // console.log(endfinal);

    // console.log(s); // string
    // console.log(e); // string

    this.setState({ startDt: s });
    this.setState({ endDt: e });
    this.setState({ loadAll: false });
    this.refs.child.loadProducts(this.state.startDt, this.state.endDt);
  }
  loadProducts() {
    var startDate = moment()
      .subtract(1, "month")
      .format("MM/DD/YYYY");
    var endDate = moment().format("MM/DD/YYYY");
    this.setState({ startDt: startDate });
    this.setState({ endDt: endDate });
    this.refs.child.loadProducts(startDate, endDate);
  }

  serachData(event) {
    // search filter
    let srchElem = document.getElementById("search")
    
      if (event.target.id == "clearSearch") {
        if (srchElem.value) {
          srchElem.value = ""
        } else {
          return
        }
      }
      if (event.keyCode === 13 || !srchElem.value) {
        srchElem.blur()
        this.filterData(srchElem.value)
    }
  }

  filterData(query) {
    this.refs.child.getAllData(query);
  }
  handleReset() {
    console.log(this.state.startDt, this.state.endDt);

    // console.log(this.refs.child.state.selectedCategory);
    let startDate = this.state.startDt;
    let endDate = this.state.endDt;
    // var startDate = moment()
    //   .subtract(1, "month")
    //   .format("MM/DD/YYYY");
    // var endDate = moment().format("MM/DD/YYYY");
    this.setState({ startDt: this.state.startDt });
    this.setState({ endDt: this.state.endDt });
    this.setState({ loadAll: true });
    this.refs.child.loadProducts(
      startDate,
      endDate,
      this.refs.child.state.selectedCategory,
      true
    );
    // console.log(this.refs.compare);
    // this.refs.compare.handleChangePage(this, 1);
  }
  passedProduct = (data, page, selectedCategory = "", selectedBrand = "") => {
    // console.log(page);
    this.setState({
      page: page,
      apage: page,
      selectedCategory: selectedCategory,
      selectedBrand: selectedBrand
    });
    // console.log(data[0].Item_Number);
    // let x = 0;
    // console.log(data[1]);
    // console.log();

    if (data.length) {
      // this.setState({ auditData: data[0].Audit_Trail }icSearch
      // this.setState({ compareData: data[0].Compare });
      // this.setState({ productName: data[0].Product_Name });
      // this.setState({ productSku: data[0].SKU });
      if (selectedBrand) {
    document.getElementById("search").value = ""
      }
      this.setState({ products: data });
      console.log(data[0].CostPrice);
      // this.setState({
      //   options: {
      //     yAxis: {
      //       plotLines: [
      //         {
      //           value: data[0].CostPrice
      //         }
      //       ]
      //     }
      //   }
      // });
      // console.log(this.state.options.yAxis.plotLines[0].value);

      // this.setState({
      //   options: {
      //     yAxis: {
      //     plotLines: {
      //       value: data[0].CostPrice
      //     }
      //   }
      //   }
      // });
      // console.log(this.state.options.yAxis);
      // setTimeout(() => {
      for (var i = 0; i < data.length; i++) {
        if (data[i].Item_Number) {
          this.handleChange(data[i].Item_Number, data[i].CostPrice);
          break;
        } else if (i == data.length - 1) {
          this.handleChange(null, null);
        }
      }
    } else {
      this.setState({ products: [] });
      this.handleChange(null, null);
    }
  };

  render() {
    const { classes } = this.props;
    const { selected, pricingselected, hasError, products } = this.state;

    return (
      <div>
        <SidebarMenu
          onProductGet={this.passedProduct}
          ref="child"
        //onHandleReset={this.handleReset.bind(this, true)}
        />
        <div className="Content">
          <Tabs defaultTab="TRENDS" horizontal>
            <div className="filter">
              <div className="search_main">

                <div className="form-group position-relative">
                  <input id="search" type="text" className="form-control" onKeyUp={this.serachData.bind(this)} placeholder="" />
                  <img src={icSearch} alt="" className="serach_ic" />
                  <img id="clearSearch" src={icClose} alt="" className="close_ic" onClick={this.serachData.bind(this)}/>
                
                </div>
              </div>
              <p>
                <span
                  className="viewall pr-2"
                  onClick={this.handleReset.bind(this, true)}
                >
                  View All
                </span>
                Filter Results:
              </p>
              <DateRangePicker
                showDropdowns
                // onEvent={this.handleEvent}
                onApply={this.handleEvent.bind(this)}
                startDate={this.state.startDate}
                endDate={this.state.endDate}
              >
                <div className="date_main">
                  <img src={icCal} className="ic_name" />
                  <span className="date_lable">From:</span> {this.state.startDt}
                  <span className="date_lable">To:</span> {this.state.endDt}
                </div>
              </DateRangePicker>
            </div>
            <TabList>
              <Tab tabFor="TRENDS" className="trends">
                PRICING TRENDS
              </Tab>
              <Tab tabFor="COMPARE" className="compare">
                COMPARE PRICES
              </Tab>
              <Tab tabFor="AUDIT" className="audit">
                AUDIT TRAIL
              </Tab>
            </TabList>
            <div className="">
              <TabPanel tabId="TRENDS">
                <div
                  className="text-center"
                  style={{ display: this.state.selected ? "none" : "block" }}
                >
                  No Data Found
                </div>
                <div
                  className="row"
                  style={{ display: !this.state.selected ? "none" : "flex" }}
                >
                  <form
                    id="two"
                    autoComplete="off"
                    className="statistics_display"
                  >
                    <label>Displaying Statistics for (Select Product): </label>
                    <FormControl error={hasError}>
                      <TextField
                        select
                        variant="outlined"
                        value={selected}
                        onChange={event =>
                          this.handleChange(
                            event.target.value,
                            event.target.CostPrice
                          )
                        }
                        displayEmpty
                        input={<Input id="name" name="name" />}
                      >
                        {products.map(elem => {
                          if (elem.Item_Number) {
                            return (
                              <MenuItem
                                classes={{ selected: classes.MenuItemSelected }}
                                className={classes.MenuItem}
                                value={elem.Item_Number}
                              >
                                {elem.Product_Name}
                              </MenuItem>
                            );
                          }
                        })}
                      </TextField>
                      {hasError && (
                        <FormHelperText>This is required!</FormHelperText>
                      )}
                    </FormControl>
                  </form>
                </div>
                <div className="col-md-12" style={{ display: !this.state.selected ? "none" : "flex" }}>
                  {/* <div><pre>{JSON.stringify(this.state.options, null, 2)}</pre></div> */}
                  <HighchartsReact
                    // highcharts={Highcharts}
                    options={this.state.options}
                  />
                </div>
              </TabPanel>
              <TabPanel tabId="COMPARE">
                <Table
                  ref="compare"
                  onpageSetFalse={page => {
                    this.setState({ page: page });
                  }}
                  data={{
                    page: this.state.page,
                    products: this.state.products,
                    selectedCategory: this.state.selectedCategory,
                    selectedBrand: this.state.selectedBrand
                    // compareData: this.state.compareData,
                    // productName: this.state.productName,
                    // productSku: this.state.productSku
                  }}
                />
              </TabPanel>
              <TabPanel tabId="AUDIT">
                <AuditTable
                  ref="audit"
                  onpageSetFalse={page => {
                    this.setState({ apage: page });
                  }}
                  data={{
                    page: this.state.apage,
                    products: this.state.products,
                    selectedCategory: this.state.selectedCategory,
                    selectedBrand: this.state.selectedBrand
                    // auditData: this.state.auditData,
                    // productName: this.state.productName,
                    // productSku: this.state.productSku
                  }}
                />
              </TabPanel>
            </div>
          </Tabs>
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(Content);
