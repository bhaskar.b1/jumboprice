// import iconSignOut from './../public/images/SidebarMenu/ic-sign-out.svg';
import "isomorphic-fetch";
import React, { Component } from "react";
import { Tab, TabList, TabPanel, Tabs } from "react-web-tabs";
import { ENVIORMENT } from "./../env";
// import "react-web-tabs/dist/react-web-tabs.css";
import HeaderAfterLogin from "./../HeaderAfterLogin/HeaderAfterLogin";
// images
import IconSearch from "./../public/images/SidebarMenu/ic_search.svg";
import "./SidebarMenu.scss";
import PropTypes from "prop-types";

class SidebarMenu extends Component {
  constructor(props) {
    super(props);
    //this.state = { active: '' };
    this.state = {
      item: [],
      clicked: false,
      isloaded: false,
      toggelClass: false,
      selectedCategory: "",
      selectedBrand: "",
      previousBrand: "",
      filterBrand: ""
    };
    this.getAllData = this.getAllData.bind(this);
    this.loadProducts = this.loadProducts.bind(this);
    this.clearBrand = this.clearBrand.bind(this);
  }

  componentDidMount() {
    // console.log(this.props);
    // var startDate = moment().subtract(1, 'month').format('DD/MM/YYYY');
    // var endDate = moment().format('DD/MM/YYYY');
    // this.loadProducts(startDate, endDate);
  }

  productDataPass(product, selectedCategory, selectedBrand) {
    // console.log(product);
    this.setState({ toggelClass: true });
    // console.log("hi");
    // console.log(product);
    this.props.onProductGet(product, 0, selectedCategory, selectedBrand);
    //    this.props.passAudit(product)
  }

  getAllData(query) {
    let all_products = [];
    if (query) {
      if (this.state.selectedBrand) {
        this.setState({ previousBrand: this.state.selectedBrand });
      }
      this.setState({ selectedBrand: "" });
      this.state.item[0].Categories.forEach(cat => {
        if (cat.Name == this.state.selectedCategory) {
          cat.Brands.forEach(brd => {
            all_products = [
              ...all_products,
              ...brd.Products.filter(elem =>
                new RegExp(query, "gi").test(elem.Product_Name)
              )
            ];
          });
        }
      });
      this.productDataPass(all_products, this.state.selectedCategory, "");
    } else {
      this.setState({ selectedBrand: this.state.previousBrand }, () => {
        this.setState({ previousBrand: "" });
        this.state.item[0].Categories.map(cat => {
          if (cat.Name == this.state.selectedCategory) {
            cat.Brands.map(brd => {
              if (brd.Name == this.state.selectedBrand) {
                this.productDataPass(
                  brd.Products,
                  this.state.selectedCategory,
                  brd.Name
                );
              }
            });
          }
        });
      });
    }
  }

  loadProducts(
    startDt = "MM/DD/YYYY",
    endDt = "MM/DD/YYYY",
    selectedCategory = "",
    view_all = false
  ) {
    // console.log(this.props);
    this.setState({
      isloaded: false
    });
    const requestOptions = {
      method: "GET",
      headers: {
        User: localStorage.getItem("Username"),
        Token: localStorage.getItem("Token")
      }
    };
    // console.log(startDt);
    let environement = ENVIORMENT.devlopmenturl;
    let apiurl = environement + "/trends?";
    if (startDt != "MM/DD/YYYY" && endDt != "MM/DD/YYYY") {
      apiurl = environement + `/trends?start=${startDt}&end=${endDt}`;
    }
    fetch(apiurl, requestOptions)
      .then(res => res.json())

      .then(json => {
        console.log(json);
        this.setState({
          isloaded: true,
          item: [json]
        });
        if (selectedCategory == "") {
          this.setState({
            selectedCategory: this.state.item[0].Categories[0].Name
          });
          this.setState({
            selectedBrand: this.state.item[0].Categories[0].Brands[0].Name
          });
          this.productDataPass(
            this.state.item[0].Categories[0].Brands[0].Products,
            this.state.item[0].Categories[0].Name,
            this.state.item[0].Categories[0].Brands[0].Name
          );
        } else {
          this.setState(
            { selectedCategory: selectedCategory, filename: selectedCategory },
            () => {
              console.log(this.state.filename);
            }
          );
          let all_products = [];
          this.state.item[0].Categories.map(cat => {
            if (cat.Name == selectedCategory) {
              if (!view_all) {
                this.setState({ selectedBrand: cat.Brands[0].Name });
                this.productDataPass(
                  cat.Brands[0].Products,
                  selectedCategory,
                  cat.Brands[0].Name
                );
              } else {
                this.setState({ selectedBrand: "" });
                cat.Brands.map(brd => {
                  all_products = all_products.concat(brd.Products);
                });
                this.productDataPass(all_products, selectedCategory, "");
              }
            }
          });
        }
      })
      .catch(err => {
        // Do something for an error here
        console.log("Error Reading data " + err);
        window.location.href = "http://18.189.254.67/JumboPriceGrabber/#/";
        // const path = "/";
        // this.props.history.push(path);
        // window.location.reload();
      });
    // this.props.onHandleReset();
  }
  // handleCheck(e) {
  //   alert(e.target);
  // }
  // handleClick = event => event.target.classList.add("click-state");
  changeCategory(category = "") {
    this.setState({ selectedCategory: category });
    this.state.item[0].Categories.map(cat => {
      if (cat.Name == category) {
        this.setState({ selectedBrand: cat.Brands[0].Name });
        this.productDataPass(
          cat.Brands[0].Products,
          category,
          cat.Brands[0].Name
        );
      }
    });
  }
  filterBrand(event, category = "") {
    this.state.item[0].Categories.map(cat => {
      cat.Brands.map(brd => {
        brd.is_show =
          brd.Name.toLowerCase().indexOf(
            this.state.filterBrand.toLowerCase()
          ) == -1;
      });
    });
    this.setState({ item: this.state.item });
  }
  clearBrand() {
    this.setState({
      selectedBrand: ""
    });
  }
  render() {
    const { match } = this.props;
    var { isloaded, item, selectedCategory, selectedBrand } = this.state;
    const style = { span: { background: "red" } };

    const auditData = item[0];
    // console.log(auditData);

    //    const {}

    if (!isloaded) {
      return <div className="loader"></div>;
    } else {
      return (
        <div>
          <HeaderAfterLogin />
          <div className="sidebar-main">
            <h4>
              VIEW BY <br /> CATEGORIES
            </h4>

            <Tabs
              onChange={category => {
                this.changeCategory(category);
              }}
              defaultTab={selectedCategory}
              vertical
            >
              <TabList>
                {item[0].Categories.map(i => (
                  <Tab tabFor={i.Name}>{i.Name}</Tab>
                ))}
              </TabList>
              <div className="subsidemenu">
                {item[0].Categories.map(item => {
                  return (
                    <TabPanel tabId={item.Name}>
                      <div className="mobile_search">
                        <img src={IconSearch} className="ic_serach" alt="" />
                        <input
                          type="text"
                          name="lastname"
                          onChange={event =>
                            this.setState({ filterBrand: event.target.value })
                          }
                          onKeyUp={e => this.filterBrand(e, item.Name)}
                          value={this.state.filterBrand}
                        />
                      </div>
                      {/*item.map(item => <span onClick={this.handelClick}>{item}</span>)*/}

                      {item.Brands.map(i => {
                        return (
                          <span
                            className={
                              (i.is_show ? "hide" : "show") +
                              (this.state.selectedBrand == i.Name
                                ? " active"
                                : "")
                            }
                            onClick={e => {
                              this.productDataPass(
                                i.Products,
                                selectedCategory,
                                i.Name
                              );
                              this.setState({ selectedBrand: i.Name });
                            }}
                            tabFor={`${i.Name}`}
                          >
                            {i.Name}
                          </span>
                        );
                      })}
                    </TabPanel>
                  );
                })}
              </div>
            </Tabs>
          </div>
        </div>
      );
    }
  }
}
export default SidebarMenu;
