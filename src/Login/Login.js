import React from "react";
import logo from "./../public/images/Login/logo.svg";
import loaderForgot from "../public/images/loaderfwd.gif";
import "isomorphic-fetch";
import "./Login.scss";
import { ENVIORMENT } from "./../env";

// import { symbol } from "prop-types";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Username: "",
      Password: "",
      Validation: "",
      ForgetValidation: "",
      Useremail: "",
      Message: "",
      isloaded1: true
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onForgetSubmit = this.onForgetSubmit.bind(this);
  }

  data;
  state = {
    loading: true
  };
  componentDidUpdate() {
    // console.log("Inside");
  }
  componentDidMount(a, b) {
    // let x = ;
    // const BODY = ;
  }
  onSubmit(e) {
    e.preventDefault();
    // console.log(this.state.Username, this.state.Password);
    const config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    };
    let user = this.state.Username;
    let pass = this.state.Password;
    let formData = new FormData();
    let environement = ENVIORMENT.devlopmenturl;
    console.log(environement);
    let url = environement + "/login";
    formData.append("user", this.state.Username);
    formData.append("pass", this.state.Password);
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ user, pass })
    };
    return (
      fetch(`${url}`, requestOptions)
        // .then(res => res.json())
        .then(res => {
          if (res.status != 200) {
            this.setState({
              Validation: "Incorrect User Name or Password"
            });
          } else {
            res.json().then(json => {
              console.log(json);
              localStorage.setItem("Username", json.username);
              localStorage.setItem("Token", json.token);
              this.props.history.push("/Content");
            });
          }
          //store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem("user", JSON.stringify(user.status));
          localStorage.setItem("username", JSON.stringify(this.state.Username));
          return user;
        })
    );
  }
  onForgetSubmit(e) {
    // e.preventDefault();
    this.setState({
      isloaded1: false
    });
    console.log(this.state.Useremail);
    let User = this.state.Useremail;
    // console.log(this.state.Username, this.state.Password);
    const config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    };
    let formData = new FormData();
    let environement = ENVIORMENT.devlopmenturl;
    console.log(environement);
    let url = environement + "/forgotPassword";
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json", User: User }
    };
    return fetch(`${url}`, requestOptions).then(res => {
      console.log(res.status);
      if (res.status != 200) {
        this.setState({
          isloaded1: true
        });

        this.setState({
          ForgetValidation: "Incorrect Email",
          Validation: "",
          Message: ""
        });
      } else {
        this.setState({
          ForgetValidation: "",
          isloaded1: true
        });
      }
      res.json().then(json => {
        if (res.status != 200) {
          console.log(json.Message);
        } else {
          this.setState({
            Message: json.Message
          });
        }
      });

      return User;
    });
  }

  MessageClear() {
    // console.log(this.state.Me)
    // this.setState({
    //   Message: "",
    //   Useremail: ""
    // });
  }
  render() {
    // this.setState({
    //   isloaded: false
    // });
    return (
      <div className="login-main">
        {/* {this.state.loading ? <div>loading...</div> : <div>person...</div>} */}
        <img src={logo} alt="Logo" className="logo" />

        <form onSubmit={this.onSubmit}>
          <span></span>
          <h2>LOG IN</h2>
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              id="email"
              placeholder="Username"
              value={this.state.Username}
              onChange={this.handleChange}
              onChange={evt =>
                this.setState({
                  Username: evt.target.value
                })
              }
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              className="form-control"
              id="pwd"
              placeholder="Password"
              value={this.state.Password}
              onChange={evt =>
                this.setState({
                  Password: evt.target.value
                })
              }
            />
            <span className="form_validation">{this.state.Validation}</span>
          </div>
          <div className="form-group form-check">
            <div className="checkbox">
              <input
                type="checkbox"
                id="checkbox_remember_me"
                name=""
                value=""
              />
              <label for="checkbox_remember_me">
                <span>Remember me</span>
              </label>
            </div>
            <a
              className="forgot_pass"
              href="ForgotPassword"
              data-toggle="modal"
              data-target="#myModal"
            >
              Forgot Password
            </a>
          </div>

          <div className="modal" id="myModal">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h4 className="modal-title">
                    Forgot Password
                    {!this.state.isloaded1 ? (
                      <div className="row">
                        <div className="col-8">
                          <img src={loaderForgot} className="img-fluid" />
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                  </h4>

                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    onClick={this.MessageClear()}
                  >
                    &times;
                  </button>
                </div>

                <div className="modal-body">
                  <div className="form-group forgot_pwd">
                    <input
                      type="text"
                      className="form-control"
                      id="email"
                      placeholder="Enter your Email"
                      value={this.state.Useremail}
                      onChange={evt =>
                        this.setState({
                          Useremail: evt.target.value
                        })
                      }
                    />
                    <span className="form_validation">
                      {this.state.ForgetValidation}
                    </span>
                    <span className="success_message">
                      {this.state.Message}
                    </span>
                  </div>
                </div>

                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn fowd_pwd"
                    onClick={this.onForgetSubmit.bind()}
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>

          <button type="submit" className="btn-login">
            Login
          </button>
        </form>
      </div>
    );
  }
}

export default Login;
