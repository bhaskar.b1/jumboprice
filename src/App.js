import React from "react";
import { Switch, Route, HashRouter, Link, withRouter } from "react-router-dom";
import IdleTimer from "react-idle-timer";
import PropTypes from "prop-types";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import Login from "./Login/Login";
import Register from "./Register/Register";
import HeaderAfterLogin from "./HeaderAfterLogin/HeaderAfterLogin";
import SidebarMenu from "./SidebarMenu/SidebarMenu";
import Content from "./Content/Content";
import AuditTable from "./AuditTable/AuditTable";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      timeout: 130 * 60 * 15,
      showModal: false,
      userLoggedIn: false,
      isTimedOut: false
    };

    this.idleTimer = null;
    this.onAction = this._onAction.bind(this);
    this.onActive = this._onActive.bind(this);
    this.onIdle = this._onIdle.bind(this);
  }

  _onAction(e) {
    console.log("Active 1");
    this.setState({ isTimedOut: false });
  }

  _onActive(e) {
    console.log("Active 2");
    this.setState({ isTimedOut: false });
  }

  _onIdle(e) {
    console.log("Idle");

    const isTimedOut = this.state.isTimedOut;
    console.log(isTimedOut);
    if (!isTimedOut) {
      console.log("Timeout");
      window.location.href = "http://18.189.254.67/JumboPriceGrabber/#/";
      window.location.reload();

      // this.props.history.push("/Register");
    } else {
      this.setState({ showModal: true });
      this.idleTimer.reset();
      this.setState({ isTimedOut: true });
    }
  }

  render() {
    const { match } = this.props;
    return (
      <>
        <IdleTimer
          ref={ref => {
            this.idleTimer = ref;
          }}
          element={document}
          onActive={this.onActive}
          onIdle={this.onIdle}
          onAction={this.onAction}
          debounce={250}
          timeout={this.state.timeout}
        />

        <HashRouter>
          <div>
            <Switch>
              <Route exact path="/" component={Login} />
              <Route path="/Register" component={Register} />
              <Route path="/HeaderAfterLogin" component={HeaderAfterLogin} />
              <Route path="/SidebarMenu" component={SidebarMenu} />
              <Route path="/Content" component={Content} />
              <Route path="/AuditTable" component={AuditTable} />
            </Switch>
          </div>
        </HashRouter>
      </>
    );
  }
}

App.propTypes = {
  match: PropTypes.any.isRequired,
  history: PropTypes.func.isRequired
};

export default withRouter(App);
