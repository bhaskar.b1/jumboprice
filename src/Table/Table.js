import Checkbox from "@material-ui/core/Checkbox";
import Paper from "@material-ui/core/Paper";
import { lighten, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import PropTypes from "prop-types";
import React, { useState } from "react";
import lifecycle, { componentDidUpdate } from "react-pure-lifecycle";
import "./Table.scss";

import { JsonToExcel } from "react-json-excel";
import * as _ from "lodash";
// import { Link } from "react-router-dom";

// import DeleteIcon from '@material-ui/icons/Delete';
// import FilterListIcon from '@material-ui/icons/FilterList';

//images
import iconXls from "./../public/images/Content/ic_xls.svg";
import { type } from "os";

// let className = "export_btn",
//   filename = "Excel-file",
//   fields = {
//     amazonprice: "Amazon",
//     carrefourprice: "Carrefour",
//     costprice: "Cost_Price",
//     datemodified: "DD.MM.YYYY",
//     description: "Product_Name",
//     jumboprice: "Jumbo",
//     jumbosku: "AHJYY89",
//     noonprice: "Noon",
//     sharafdgprice: "SharafDG",
//     timemodifie: "00.00.00"
//   },
//   style = {
//     padding: "5px"
//   },
//   dataE = [];

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === "desc"
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const JSONToCSVConvertor = (JSONData, fileName, ShowLabel, selected) => {
  let JSONDatas = [];
  JSONData.map(data => {
    if (selected.indexOf(data.index) !== -1) {
      JSONDatas.push({
        "Product Description": data.description,
        "Jumbo SKU": data.jumbosku,
        "Jumbo Price": data.jumboprice,
        "Cost Price": data.costprice,
        "Carrefour Price": data.carrefourprice,
        "SharafDG Price": data.sharafdgprice,
        "Amazon Price": data.amazonprice,
        "Noon Price": data.noonprice,
        "Lowest Competitor": data.competitorname,
        "Lowest Price": data.competitorprice,
        "Date Modified": data.datemodified,
        "Time Modified": data.timemodified
        // modifiedb: "Username"
      });
    }
  });
  if (JSONDatas.length == 0) {
    alert("Please select checkbox");
    return false;
  }
  // console.log(selected);
  // console.log(JSONData);
  // console.log(JSONDatas);
  //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
  var arrData =
    typeof JSONDatas != "object" ? JSON.parse(JSONDatas) : JSONDatas;
  var CSV = "";
  //Set Report title in first row or line
  // CSV += ReportTitle + "\r\n\n";
  //This condition will generate the Label/Header
  if (ShowLabel) {
    var row = "";
    //This loop will extract the label from 1st index of on array
    for (var index in arrData[0]) {
      //Now convert each value to string and comma-seprated
      row += index + ",";
    }
    row = row.slice(0, -1);
    //append Label row with line break
    CSV += row + "\r\n";
  }
  //1st loop is to extract each row
  for (var i = 0; i < arrData.length; i++) {
    var row = "";
    //2nd loop will extract each column and convert it in string comma-seprated
    for (var index in arrData[i]) {
      row += '"' + arrData[i][index] + '",';
    }
    row.slice(0, row.length - 1);
    //add a line break after each row
    CSV += row + "\r\n";
  }
  if (CSV == "") {
    alert("Invalid data");
    return;
  }
  //Generate a file name
  // var fileName = "MyReport_";
  //this will remove the blank-spaces from the title and replace it with an underscore
  // fileName += ReportTitle.replace(/ /g, "_");
  //Initialize file format you want csv or xls
  var uri = "data:text/csv;charset=utf-8," + escape(CSV);

  // Now the little tricky part.
  // you can use either>> window.open(uri);
  // but this will not work in some browsers
  // or you will not get the correct file extension
  //this trick will generate a temp <a /> tag
  var link = document.createElement("a");
  link.href = uri;
  //set the visibility hidden so it will not effect on your web-layout
  link.style = "visibility:hidden";
  link.download = fileName + ".csv";
  //this part will append the anchor tag and remove it after automatic click
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};

const headCells = [
  {
    id: "description",
    numeric: false,
    disablePadding: true,
    label: "PRODUCT DESCRIPTION",
    textalignleft: true,
    sort: true
  },
  {
    id: "jumbosku",
    numeric: false,
    disablePadding: false,
    label: "Jumbo\r\nSKU",
    textalignleft: false,
    sort: false
  },
  {
    id: "jumboprice",
    numeric: true,
    disablePadding: false,
    label: "Jumbo\r\nPrice",
    textalignleft: false,
    sort: false
  },
  {
    id: "costprice",
    numeric: true,
    disablePadding: false,
    label: "Cost\r\nPrice",
    textalignleft: false,
    sort: false
  },
  {
    id: "carrefourprice",
    numeric: true,
    disablePadding: false,
    label: "Carrefour\r\nPrice",
    textalignleft: false,
    sort: false
  },
  {
    id: "sharafdgprice",
    numeric: true,
    disablePadding: false,
    label: "SharafDG\r\nPrice",
    textalignleft: false,
    sort: false
  },
  {
    id: "amazonprice",
    numeric: true,
    disablePadding: false,
    label: "Amazon\r\nPrice",
    textalignleft: false,
    sort: false
  },
  {
    id: "noonprice",
    numeric: true,
    disablePadding: false,
    label: "Noon\r\nPrice",
    textalignleft: false,
    sort: false
  },
  {
    id: "competitorname",
    numeric: false,
    disablePadding: false,
    label: "Lowest\r\nCompetitor",
    textalignleft: false,
    sort: false
  },
  {
    id: "competitorprice",
    numeric: true,
    disablePadding: false,
    label: "Lowest\r\nPrice",
    textalignleft: false,
    sort: true
  },
  {
    id: "datemodified",
    numeric: false,
    disablePadding: false,
    label: "Date\r\nModified",
    textalignleft: false,
    sort: false
  },
  {
    id: "timemodified",
    numeric: false,
    disablePadding: false,
    label: "Time\r\nModified",
    textalignleft: false,
    sort: false
  }
];

function EnhancedTableHead(props) {
  const {
    classes,
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort
  } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };
  //   const createSortHandler = property => event => {
  //     Qtm
  //   };

  return (
    <TableHead className={classes.tablehead}>
      <TableRow>
        <TableCell padding="checkbox" className={classes.tableheadcheckbox}>
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={numSelected === rowCount}
            className={classes.checkbox}
            inputProps={{ "aria-label": "select all desserts" }}
            onChange={onSelectAllClick}
          />
        </TableCell>
        {headCells.map(headCell => (
          // <TableCell
          //   className={classes.tabletitle + (headCell.sort ? "show" : "hide")}
          //   key={headCell.id}
          //   align={headCell.textalignleft ? "left" : "center"}
          //   padding={headCell.disablePadding ? "none" : "default"}>
          //   {headCell.label}
          // </TableCell>
          <TableCell
            className={classes.tabletitle}
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}>
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={order}
              onClick={createSortHandler(headCell.id)}
              hideSortIcon={true}>
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  rowCount: PropTypes.number.isRequired
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1)
  },
  highlight:
    theme.palette.type === "light"
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85)
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark
      },
  spacer: {
    flex: "1 1 100%"
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: "0 0 auto"
  }
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return "";
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired
};

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3)
  },
  toolbar: {
    boxShadow: "none"
  },
  tablehead: {
    background: "#e2e9f05c",
    "& tr th:nth-child(2)": {
      borderLeft: "none"
    }
  },
  tableheadcheckbox: {
    borderBottom: "none",
    borderLeft: "none",
    padding: "10px"
  },
  tabletitle: {
    fontWeight: "bold",
    color: "#174a84",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px",
    whiteSpace: "pre-line",
    textAlign: "center"
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
    boxShadow: "none"
  },
  table: {
    minWidth: 750
  },
  row: {
    borderBottom: "none"
  },
  checkboxcell: {
    borderBottom: "none",
    minWidth: "40px",
    padding: "10px"
  },
  checkbox: {
    width: "13px",
    height: "13px",
    borderRadius: "3px",
    background: "#fff",
    color: "#0086f8 !important",
    borderBottom: "none"
  },
  description: {
    fontWeight: "500",
    color: "#164883",
    borderBottom: "none",
    padding: "10px"
  },
  costprice: {
    fontWeight: "500",
    textAlign: "center",
    color: "#164883",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  jumbosku: {
    fontWeight: "500",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  jumboprice: {
    fontWeight: "500",
    textAlign: "center",
    color: "#00c853",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  carrefourprice: {
    fontWeight: "500",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  sharafdgprice: {
    fontWeight: "500",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  amazonprice: {
    fontWeight: "500",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  noonprice: {
    fontWeight: "500",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  competitorname: {
    fontWeight: "500",
    textAlign: "center",
    color: "#164883",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  competitorprice: {
    fontWeight: "500",
    textAlign: "center",
    color: "#0086f8",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  datemodified: {
    fontWeight: "500",
    textAlign: "center",
    color: "#164883",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  timemodified: {
    fontWeight: "500",
    textAlign: "center",
    color: "#164883",
    borderBottom: "none",
    borderLeft: "solid 0.25px #0086F8",
    padding: "10px"
  },
  tableWrapper: {
    overflowX: "auto"
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1
  }
}));
// let value = 0;
// console.log(value);

function EnhancedTable(props) {
  
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("description");
  const [selected, setSelected] = useState([]);
  let [page, setPage] = useState(0);
  // console.log(props);

  // console.trace();
  // console.log(props.datavalu1);
  const { count } = props;
  // if (page > 0) {

  // }
  // const newLastPage = Math.max(0, Math.ceil(page / rowsPerPage) - 1);
  // console.log(newLastPage);

  // if (props.datavalu1 == true) {
  //   setPage(0);
  //   page = 0;
  //   props.datavalu1 = false;
  // }
  // console.log(props.datavalu1);

  // console.log(localStorage.getItem("Token"));
  // <Link to="/"></Link>;
  let className = "export_btn",
    filename = "Compare Prices - " + props.data.selectedCategory,
    dataE = [];

  // rows.forEach(i => {
  //   dataE.push(i);
  // });
  let rows = [];
  // console.log(_.cloneDeep(props.data.products));
  let ri = 0;
  let amazonurl = "";
  props.data.products.map(pro => {
    // pro.Compare.map(x => console.log(x.Amazon_Url))
    console.log(pro);
    pro.Compare.map(x => {
      // this.amazonurl = x.Amazon_Url;
      rows.push({
        index: ri++,
        description: pro.Product_Name,
        jumbosku: pro.SKU,
        jumboprice: x.Jumbo,
        costprice: x.Cost_Price,
        carrefourprice: x.Carrefour,
        sharafdgprice: x.SharafDG,
        amazonprice: x.Amazon,
        amazonLink: x.Amazon_Url,
        noonLink: x.Noon_Url,
        carrefourLink: x.Carrefour_Url,
        sharafLink: x.Sharaf_Url,
        noonprice: x.Noon,
        competitorname: x.Lowest_Competitor_Name,
        competitorprice: x.Lowest_Competitor_Price,
        datemodified: x.Date,
        timemodified: x.Time
        // modifiedb: "Username"
      });
    });
  });
  const classes = useStyles();

  //   const [dense, setDense] = React.useState(false);

  const handleRequestSort = (event, property) => {
    const isDesc = orderBy === property && order === "desc";
    setOrder(isDesc ? "asc" : "desc");
    setOrderBy(property);
  };

  const handleSelectAllClick = event => {
    if (event.target.checked) {
      const newSelecteds = rows.map(n => n.index);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, description) => {
    console.log(event);
    console.log(description);
    const selectedIndex = selected.indexOf(description);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, description);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    // console.log(event);
    console.log(newPage);
    console.log(props.data.page);
    // if (props.data.page != newPage)
    setPage(newPage);
    props.onpageSetFalse(newPage);
  };

  const handleChangeRowsPerPage = event => {
    console.log("Inside how");
    setRowsPerPage(+event.target.value);
    setPage(0);
    props.onpageSetFalse(0);
  };

  const isSelected = description => selected.indexOf(description) !== -1;

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  const [pageSizes] = React.useState([5, 10, 15, 0]);
  // handleClick1() {

  // }
  let all = [5, 10, 25, 50, 100, 200];
  if (props.data.page != page) {
    handleChangePage(null, 0);
  }
  return (
    <div className="row">
      <div
        className="col text-center"
        style={{ display: rows.length > 0 ? "none" : "block" }}>
        No Data Found
      </div>
      <div
        className="table-main col"
        style={{ display: rows.length == 0 ? "none" : "flex" }}>
        <Paper className={classes.paper}>
          <EnhancedTableToolbar
            numSelected={selected.length}
            className={classes.toolbar}
          />
          <div className={classes.tableWrapper}>
            <Table className={classes.table} aria-labelledby="tableTitle">
              <EnhancedTableHead
                classes={classes}
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}
              />
              <TableBody>
                {stableSort(rows, getSorting(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    const isItemSelected = isSelected(row.index);
                    const labelId = `enhanced-table-checkbox-${row.index}`;

                    return (
                      <TableRow
                        hover
                        onClick={event => handleClick(event, row.index)}
                        role="checkbox"
                        aria-checked={isItemSelected}
                        tabIndex={-1}
                        key={row.index}
                        selected={isItemSelected}
                        className={classes.row}
                        style={
                          index % 2
                            ? { background: "#e2e9f05c" }
                            : { background: "#fff" }
                        }>
                        <TableCell
                          padding="checkbox"
                          className={classes.checkboxcell}>
                          <Checkbox
                            className={classes.checkbox}
                            checked={isItemSelected}
                            inputProps={{ "aria-labelledby": labelId }}
                          />
                        </TableCell>
                        <TableCell
                          className={classes.description}
                          component="th"
                          id={labelId}
                          scope="row"
                          padding="none">
                          {row.description}
                        </TableCell>

                        <TableCell align="right" className={classes.jumbosku}>
                          {row.jumbosku}
                        </TableCell>
                        <TableCell align="right" className={classes.jumboprice}>
                          {row.jumboprice}
                        </TableCell>
                        <TableCell align="right" className={classes.costprice}>
                          {row.costprice}
                        </TableCell>
                        <TableCell
                          align="right"
                          className={classes.carrefourprice}>
                          {row.carrefourprice == 0 ? (
                            <span className="url_null">{row.carrefourprice}</span>
                          ) : (
                              <a href={row.carrefourLink} target="_blank">{row.carrefourprice}</a>
                            )}
                          {/* <a href={row.carrefourLink} target="_blank">{row.carrefourprice}</a> */}
                        </TableCell>
                        <TableCell
                          align="right"
                          className={classes.sharafdgprice}>
                          {row.sharafdgprice == 0 ? (
                            <span className="url_null">{row.sharafdgprice}</span>
                          ) : (
                              <a href={row.sharafLink} target="_blank">{row.sharafdgprice}</a>
                            )}
                          {/* <a href={row.sharafLink} target="_blank">{row.sharafdgprice}</a> */}
                        </TableCell>
                        <TableCell
                          align="right"
                          className={classes.amazonprice}>
                          {row.amazonprice == 0 ? (
                            <span className="url_null">{row.amazonprice}</span>
                          ) : (
                              <a href={row.amazonLink} target="_blank">{row.amazonprice}</a>
                            )}
                          {/* <a href={row.amazonLink} target="_blank">{row.amazonprice}</a> */}
                        </TableCell>
                        <TableCell align="right" className={classes.noonprice}>
                          {row.noonprice == 0 ? (
                            <span className="url_null">{row.noonprice}</span>
                          ) : (
                              <a href={row.noonLink} target="_blank">{row.noonprice}</a>
                            )}
                          {/* <a href={row.noonLink} target="_blank">{row.noonprice}</a> */}
                        </TableCell>
                        <TableCell
                          align="right"
                          className={classes.competitorname}>
                          {row.competitorname}
                        </TableCell>
                        <TableCell
                          align="right"
                          className={classes.competitorprice}>
                          {row.competitorprice}
                        </TableCell>
                        <TableCell
                          align="right"
                          className={classes.datemodified}>
                          {row.datemodified}
                        </TableCell>
                        <TableCell
                          align="right"
                          className={classes.timemodified}>
                          {row.timemodified}
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </div>

          <div className="row align-items-center float-right">
            <div className="">
              <span
                className="export_btn"
                onClick={e => {
                  JSONToCSVConvertor(rows, filename, true, selected);
                }}>
                <img src={iconXls} className="align-text-top pr-2" />
                EXPORT REPORT
              </span>
            </div>
            <TablePagination
              rowsPerPageOptions={[10, 20, 30]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{
                "aria-label": "previous page"
              }}
              nextIconButtonProps={{
                "aria-label": "next page"
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </div>
        </Paper>
      </div>
    </div >
  );
}
export default EnhancedTable;
